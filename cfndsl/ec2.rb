# frozen_string_literal: true

require 'cfndsl'

CloudFormation do
  dc_count = external_parameters[:dc_count] || 1
  deletion_policy = false
  Description 'Windows EC2 Instance'

  Parameter(:VPCSecurityGroups) do
    Type 'List<AWS::EC2::SecurityGroup::Id>'
  end

  Parameter(:SubnetIds) do
    Type 'List<AWS::EC2::Subnet::Id>'
  end

  Parameter(:InstanceType) do
    Type 'String'
    Default 't2.medium'
    AllowedValues ['t2.medium', 'm4.large', 'm4.xlarge', 'm4.2xlarge', 'm4.4xlarge']
  end

  Parameter(:KeyName) do
    Type 'AWS::EC2::KeyPair::KeyName'
  end

  Parameter(:ImageId) do
    Type 'AWS::EC2::Image::Id'
    Default 'ami-ab928fc8'
  end

  Parameter(:VpcId) do
    Type 'AWS::EC2::VPC::Id'
  end

  EC2_SecurityGroup(:rdpsg) do
    GroupDescription 'RDP'
    VpcId Ref(:VpcId)
  end

  EC2_SecurityGroupIngress(:rdpingress) do
    FromPort 3389
    ToPort 3389
    IpProtocol 'tcp'
    SourceSecurityGroupId Ref(:rdpsg)
    GroupId Ref(:rdpsg)
  end

  (0...dc_count.to_i).each do |i|
    EC2_Instance("myinstance#{i}") do
      DisableApiTermination true
      ImageId Ref(:ImageId)
      InstanceType Ref(:InstanceType)
      KeyName Ref(:KeyName)
      Monitoring true
      SecurityGroupIds FnSplit(',', FnJoin(',', [FnJoin(',', Ref(:VPCSecurityGroups)), FnGetAtt(:rdpsg, 'GroupId')]))
      SubnetId FnSelect(i, Ref(:SubnetIds))
      Tags [{ Key: 'Environment', Value: 'Sitecore' }, { Key: 'Name', Value: "Sitecore CD#{i}" }]
      DeletionPolicy 'Retain' if deletion_policy
    end
  end
end
