# frozen_string_literal: true

require 'cfndsl'

CloudFormation do
  dc_count = external_parameters[:dc_count] || 2
  build_database = true
  build_database = false if external_parameters[:build_database] == 'false'
  deletion_policy = true
  deletion_policy = false if external_parameters[:deletion_policy] == 'false'
  Description 'sitecore multiaz rds'

  Parameter(:VPCSecurityGroups) do
    Type 'List<AWS::EC2::SecurityGroup::Id>'
  end

  Parameter(:SubnetIds) do
    Type 'List<AWS::EC2::Subnet::Id>'
  end

  Parameter(:DBInstanceClass) do
    Type 'String'
    Default 'db.m3.medium'
    AllowedValues ['db.m3.medium', 'db.m4.large', 'db.m4.xlarge', 'db.m4.2xlarge', 'db.m4.4xlarge']
  end

  Parameter(:InstanceType) do
    Type 'String'
    Default 't2.medium'
    AllowedValues ['t2.medium', 'm4.large', 'm4.xlarge', 'm4.2xlarge', 'm4.4xlarge']
  end

  Parameter(:KeyName) do
    Type 'AWS::EC2::KeyPair::KeyName'
  end

  Parameter(:ImageId) do
    Type 'AWS::EC2::Image::Id'
    Default 'ami-ab928fc8'
  end

  Parameter(:VpcId) do
    Type 'AWS::EC2::VPC::Id'
  end

  EC2_SecurityGroup(:sitecoresg) do
    GroupDescription 'sitecore cd to rds'
    VpcId Ref(:VpcId)
  end

  EC2_SecurityGroupEgress(:sitecoresgegress) do
    FromPort 1433
    ToPort 1433
    IpProtocol 'tcp'
    DestinationSecurityGroupId Ref(:sitecoresg)
    GroupId Ref(:sitecoresg)
  end

  EC2_SecurityGroupIngress(:sitecoresgingress) do
    FromPort 1433
    ToPort 1433
    IpProtocol 'tcp'
    SourceSecurityGroupId Ref(:sitecoresg)
    GroupId Ref(:sitecoresg)
  end

  if build_database
    RDS_DBInstance(:sitecoredb) do
      AllocatedStorage '200'
      AllowMajorVersionUpgrade false
      AutoMinorVersionUpgrade true
      BackupRetentionPeriod '1'
      CopyTagsToSnapshot true
      DBInstanceClass Ref(:DBInstanceClass)
      DBParameterGroupName Ref(:parametergroup)
      DBSubnetGroupName Ref(:subnetgroup)
      Engine 'sqlserver-se'
      EngineVersion '12.00.4422.0.v1'
      LicenseModel 'license-included'
      MasterUserPassword 'password'
      MasterUsername 'dbadmin'
      MultiAZ true
      OptionGroupName Ref(:optiongroup)
      PubliclyAccessible false
      StorageType 'gp2'
      Timezone 'AUS Eastern Standard Time'
      VPCSecurityGroups FnSplit(',', FnJoin(',', [FnJoin(',', Ref(:VPCSecurityGroups)), FnGetAtt(:sitecoresg, 'GroupId')]))
      Tags [{ Key: 'Environment', Value: 'Sitecore' }, { Key: 'Name', Value: 'Sitecore DB' }]
      DeletionPolicy 'Retain' if deletion_policy
    end

    Route53_RecordSet(:dbrecordset) do
      HostedZoneId Ref(:sitecorelocal)
      Name 'db.sitecore.local'
      TTL 300
      Type 'CNAME'
      ResourceRecords [FnGetAtt(:sitecoredb, 'Endpoint.Address')]
      DeletionPolicy 'Retain' if deletion_policy
    end
  end

  RDS_OptionGroup(:optiongroup) do
    EngineName 'sqlserver-se'
    MajorEngineVersion '12.00'
    OptionConfigurations []
    OptionGroupDescription 'sitecore option group'
    Tags [{ Key: 'Environment', Value: 'Sitecore' }]
    DeletionPolicy 'Retain' if deletion_policy
  end

  RDS_DBParameterGroup(:parametergroup) do
    Description 'sitecore parameter group'
    Family 'sqlserver-se-12.0'
    Tags [{ Key: 'Environment', Value: 'Sitecore' }]
    DeletionPolicy 'Retain' if deletion_policy
  end

  RDS_DBSubnetGroup(:subnetgroup) do
    DBSubnetGroupDescription 'sitecore subnetgroup'
    SubnetIds Ref(:SubnetIds)
    Tags [{ Key: 'Environment', Value: 'Sitecore' }]
    DeletionPolicy 'Retain' if deletion_policy
  end

  (0...dc_count.to_i).each do |i|
    EC2_Instance("sitecorecd#{i}") do
      DisableApiTermination true
      ImageId Ref(:ImageId)
      InstanceType Ref(:InstanceType)
      KeyName Ref(:KeyName)
      Monitoring true
      SecurityGroupIds FnSplit(',', FnJoin(',', [FnJoin(',', Ref(:VPCSecurityGroups)), FnGetAtt(:sitecoresg, 'GroupId')]))
      SubnetId FnSelect(i, Ref(:SubnetIds))
      Tags [{ Key: 'Environment', Value: 'Sitecore' }, { Key: 'Name', Value: "Sitecore CD#{i}" }]
      DeletionPolicy 'Retain' if deletion_policy
    end
    CloudWatch_Alarm("restartalarm#{i}") do
      Namespace 'AWS/EC2'
      MetricName 'StatusCheckFailed_System'
      Statistic 'Minimum'
      Period '60'
      EvaluationPeriods '15'
      ComparisonOperator 'GreaterThanThreshold'
      Threshold '0'
      AlarmActions [FnJoin('', ['arn:aws:automate:', Ref('AWS::Region'), ':ec2:recover'])]
      Dimensions [{ 'Name' => 'InstanceId', 'Value' => Ref("sitecorecd#{i}") }]
      DeletionPolicy 'Retain' if deletion_policy
    end
    Route53_RecordSet("cdrecordset#{i}") do
      HostedZoneId Ref(:sitecorelocal)
      Name "cd#{i}.sitecore.local"
      TTL 300
      Type 'A'
      ResourceRecords [FnGetAtt("sitecorecd#{i}", 'PrivateIp')]
      DeletionPolicy 'Retain' if deletion_policy
    end
  end
  Route53_HostedZone(:sitecorelocal) do
    HostedZoneTags [{ Key: 'Environment', Value: 'Sitecore' }]
    Name 'sitecore.local'
    VPCs [{ VPCId: Ref(:VpcId), VPCRegion: Ref('AWS::Region') }]
    DeletionPolicy 'Retain' if deletion_policy
  end
end
